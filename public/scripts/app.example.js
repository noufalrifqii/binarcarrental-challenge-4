class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");

    this.driver = document.getElementById("driver");
    this.tanggal = document.getElementById("date");
    this.waktu = document.getElementById("time");
    this.penumpang = document.getElementById("penumpang");
  }

  // async init() {
  //   await this.load();

  //   // Register click listener
  //   this.clearButton.onclick = this.clear;
  //   this.loadButton.onclick = this.run;
  // }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.classList.add("cari-mobil", "col-lg-4", "col-md-6", "col-sm-12");
      node.innerHTML = car.render();
      this.carContainerElement.style.margin = "0px 20px";
      this.carContainerElement.appendChild(node);
    });
  };

  async load(penumpang) {
    this.driver = document.getElementById("driver").value;
    this.tanggal = document.getElementById("date").value;
    this.waktu = document.getElementById("time").value;
    var tipeDriver = null;
    if (this.driver == "Dengan Sopir") {
      tipeDriver = true;
    } else if (this.driver == "Tanpa Sopir") {
      tipeDriver = false;
    }
    const datetime = new Date(`${this.tanggal} ${this.waktu.substr(0, 5)}`);
    const beforeEpochTime = datetime.getTime();
    const cars = await Binar.listCars((item) => {
      const itemDate = new Date(item.availableAt);
      const date_filter = itemDate.getTime() < beforeEpochTime;
      return item.available == tipeDriver && date_filter && item.capacity >= Number(penumpang);
    });
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
