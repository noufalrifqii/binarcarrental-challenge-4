class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({ id, plate, manufacture, model, image, rentPerDay, capacity, description, transmission, available, type, year, options, specs, availableAt }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
      <div class="card card-mobil" style="border-radius: 10px; box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.15);">
        <img src="${this.image}" class="card-img-top" height="250px" style alt="${this.manufacture}" />
        <div class="card-body">
          <p class="card-text" style="font-weight:400;">${this.manufacture} ${this.model} / ${this.type}</p>
          <p class="card-title" style="font-weight: bold">Rp ${this.rentPerDay} / hari</p>
          <p class="card-text">${this.description.slice(0, 35)}</p>
          <p class="card-text">
            <img src="images/fi_users.png" alt="" />
            ${this.capacity} Penumpang
          </p>
          <p class="card-text">
            <img src="images/fi_settings.png" alt="" />
            ${this.transmission}
          </p>
          <p class="card-text">
            <img src="images/fi_calendar.png" alt="" />
            Tahun ${this.year}
          </p>
          <a href="#" class="btn btn-success" style="width: 100%">Pilih Mobil</a>
        </div>
      </div>
    `;
  }
}
