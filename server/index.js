const http = require("http");
const fs = require("fs");
const path = require("path");
const url = require("url");
const port = 8000;
const PUBLIC_DIRECTORY = path.join(__dirname, "../public");

http
  .createServer((req, res) => {
    let requesturl = req.url;
    switch (requesturl) {
      case "/" || "index":
        requesturl = "/index.html";
        break;
      case "/car":
        requesturl = "/cari.html";
        break;
      default:
        requesturl = req.url;
        break;
    }
    const parseURL = url.parse(requesturl);
    const pathName = `${parseURL.pathname}`;
    const extension = path.parse(pathName).ext;
    const absolutePath = path.join(PUBLIC_DIRECTORY, pathName);
    const mapContent = {
      ".html": "text/html",
      ".css": "text/css",
      ".jpg": "image/jpeg",
      ".png": "image/png",
      ".js": "text/javascript",
    };

    fs.exists(absolutePath, (exist) => {
      if (!exist) {
        res.writeHead(404);
        res.end("FILE NOT FOUND");
        return;
      }
    });

    fs.readFile(absolutePath, (err, data) => {
      if (err) {
        res.statusCode = 500;
        res.end("FILE NOT FOUND");
      } else {
        res.setHeader("Content-Type", mapContent[extension] || "text/plain");
        res.end(data);
      }
    });
  })
  .listen(port, () => {
    console.log(`http://localhost:${port}`);
  });
